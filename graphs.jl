using DataFrames
using Statistics
using Plots

colorado_searcher_1 = DataFrame(
    Branching = Bool[],
    Name = String[],
    Rates = Array{Float64, 1}[],
)

push!(colorado_searcher_1, (
    true,
    "Sample All (#1)",
    [150566.057, 162222.495, 160484.262],
))

push!(colorado_searcher_1, (
    true,
    "Sample Random (#2)",
    [150795.787, 152869.553, 153876.574],
))

push!(colorado_searcher_1, (
    true,
    "Std Dedup (#3)",
    [56221.388, 56007.516, 56502.477],
))

push!(colorado_searcher_1, (
    true,
    "Hashbrown Dedup (#4)",
    [47327.666, 50826.386, 51018.896],
))


push!(colorado_searcher_1, (
    false,
    "Non-Branching (#5)",
    [576718.730, 570689.664, 578309.909],
))

bar(
    colorado_searcher_1[:Name],
    mean.(colorado_searcher_1[:Rates]),
    ylab="Average Samples Completed Per Second",
    title="Roundabout Sampling Throughput",
    xrotation=10,
    legend=:none)
savefig("graphics/roundabout_throughput.pdf")

cliq_names = Dict(
    "channel_stealy" => "N Shared Channels",
    "disgusting_chain" => "1 Global Channel",
    "stealy_stealy" => "N Work-Stealing Queues",
)

latency = [
    1 * 60 + 42 + 25 / 100.0,
    1 * 60 + 25 + 11 / 100.0,
    1 * 60 + 12 + 18 / 100.0,
    0 * 60 + 51 + 96 / 100.0,
    0 * 60 + 37 + 24 / 100.0,
    0 * 60 + 31 + 72 / 100.0,
    0 * 60 + 28 + 53 / 100.0,
    0 * 60 + 28 + 77 / 100.0,
    0 * 60 + 27 + 56 / 100.0,
    0 * 60 + 46 + 58 / 100.0,
    1 * 60 + 49 + 17 / 100.0,
    1 * 60 + 03 + 54 / 100.0,
    0 * 60 + 48 + 96 / 100.0,
    0 * 60 + 30 + 68 / 100.0,
    0 * 60 + 21 + 19 / 100.0,
    0 * 60 + 19 + 37 / 100.0,
    0 * 60 + 19 + 16 / 100.0,
    0 * 60 + 18 + 44 / 100.0,
    0 * 60 + 18 + 27 / 100.0,
    0 * 60 + 25 + 04 / 100.0,
    1 * 60 + 43 + 90 / 100.0,
    1 * 60 + 25 + 57 / 100.0,
    1 * 60 + 10 + 27 / 100.0,
    0 * 60 + 48 + 80 / 100.0,
    0 * 60 + 33 + 49 / 100.0,
    0 * 60 + 27 + 66 / 100.0,
    0 * 60 + 25 + 84 / 100.0,
    0 * 60 + 25 + 36 / 100.0,
    0 * 60 + 25 + 96 / 100.0,
    0 * 60 + 38 + 20 / 100.0,
]
max_cliq_1 = DataFrame(
    Impl = vcat((fill(name, (10)) for name=["channel_stealy", "disgusting_chain", "stealy_stealy"])...),
    Threads = repeat([1, 2, 3, 6, 12, 18, 24, 28, 32, 48], (3)),
    Latency = latency,
)

latency = [
    0 * 60 + 16 + 42 / 100.0,
    1 * 60 + 15 + 29 / 100.0,
    4 * 60 + 37 + 47 / 100.0,
    0 * 60 + 11 + 58 / 100.0,
    1 * 60 + 26 + 55 / 100.0,
    5 * 60 + 56 + 44 / 100.0,
    0 * 60 + 13 + 09 / 100.0,
    0 * 60 + 44 + 63 / 100.0,
    2 * 60 + 16 + 97 / 100.0,
]
max_cliq_2 = DataFrame(
    Impl = vcat((fill(name, (3)) for name=["channel_stealy", "disgusting_chain", "stealy_stealy"])...),
    Jettison = repeat([0, 1, 2], (3)),
    Latency = latency,
)

latency = [
    0 * 60 + 15 + 86 / 100.0,
    0 * 60 + 15 + 55 / 100.0,
    0 * 60 + 15 + 20 / 100.0,
    0 * 60 + 14 + 78 / 100.0,
    0 * 60 + 15 + 93 / 100.0,
    0 * 60 + 12 + 58 / 100.0,
    0 * 60 + 12 + 09 / 100.0,
    0 * 60 + 11 + 90 / 100.0,
    0 * 60 + 10 + 93 / 100.0,
    0 * 60 + 11 + 66 / 100.0,
    0 * 60 + 14 + 08 / 100.0,
    0 * 60 + 14 + 70 / 100.0,
    0 * 60 + 13 + 86 / 100.0,
    0 * 60 + 14 + 95 / 100.0,
    0 * 60 + 15 + 87 / 100.0,
]
max_cliq_3 = DataFrame(
    Impl = vcat((fill(name, (5)) for name=["channel_stealy", "disgusting_chain", "stealy_stealy"])...),
    Normalization = repeat([-1, -0.5, 0.0, 0.5, 1.0], (3)),
    Latency = latency,
)

latency = [
    3 * 60 + 07 + 04 / 100.0,
    2 * 60 + 17 + 12 / 100.0,
    1 * 60 + 59 + 04 / 100.0,
    0 * 60 + 59 + 41 / 100.0,
    0 * 60 + 28 + 92 / 100.0,
    0 * 60 + 43 + 10 / 100.0,
    0 * 60 + 36 + 51 / 100.0,
    0 * 60 + 32 + 93 / 100.0,
    0 * 60 + 24 + 48 / 100.0,
    0 * 60 + 18 + 15 / 100.0,
    2 * 60 + 38 + 53 / 100.0,
    2 * 60 + 17 + 82 / 100.0,
    1 * 60 + 56 + 97 / 100.0,
    0 * 60 + 57 + 81 / 100.0,
    0 * 60 + 26 + 19 / 100.0,
]
max_cliq_4 = DataFrame(
    Impl = vcat((fill(name, (5)) for name=["channel_stealy", "disgusting_chain", "stealy_stealy"])...),
    Theshold = repeat([0.1, 0.25, 0.5, 0.75, 0.9], (3)),
    Latency = latency,
)

latency = [
    0 * 60 + 00 + 51 / 100.0,
    0 * 60 + 03 + 52 / 100.0,
    0 * 60 + 08 + 27 / 100.0,
    0 * 60 + 15 + 71 / 100.0,
    0 * 60 + 21 + 88 / 100.0,
    0 * 60 + 29 + 77 / 100.0,
    0 * 60 + 00 + 46 / 100.0,
    0 * 60 + 02 + 72 / 100.0,
    0 * 60 + 06 + 76 / 100.0,
    0 * 60 + 11 + 00 / 100.0,
    0 * 60 + 15 + 50 / 100.0,
    0 * 60 + 18 + 05 / 100.0,
    0 * 60 + 00 + 55 / 100.0,
    0 * 60 + 03 + 63 / 100.0,
    0 * 60 + 07 + 18 / 100.0,
    0 * 60 + 14 + 36 / 100.0,
    0 * 60 + 19 + 70 / 100.0,
    0 * 60 + 25 + 02 / 100.0,
]
max_cliq_5 = DataFrame(
    Impl = vcat((fill(name, (6)) for name=["channel_stealy", "disgusting_chain", "stealy_stealy"])...),
    Samples = repeat([1, 10, 25, 50, 75, 100], (3)),
    Latency = latency,
)

plot(xlabel="# Threads", ylabel="Latency (s)", title="Latency of Max-Clique Search")
for impl=levels(max_cliq_1.Impl)
    plot!(
        max_cliq_1[max_cliq_1.Impl .== impl, :Threads],
        max_cliq_1[max_cliq_1.Impl .== impl, :Latency],
        lab=cliq_names[impl])
end
savefig("graphics/cliq_latency.pdf")

plot(xlabel="Max Jettison Depth", ylabel="Latency (s)", title="Latency of Max-Clique Search")
for impl=levels(max_cliq_2.Impl)
    plot!(
        max_cliq_2[max_cliq_2.Impl .== impl, :Jettison],
        max_cliq_2[max_cliq_2.Impl .== impl, :Latency],
        lab=cliq_names[impl])
end
savefig("graphics/cliq_jettison.pdf")

plot(xlabel="Cliquiness Normalization Factor", ylabel="Latency (s)", title="Latency of Max-Clique Search")
for impl=levels(max_cliq_3.Impl)
    plot!(
        max_cliq_3[max_cliq_3.Impl .== impl, :Normalization],
        max_cliq_3[max_cliq_3.Impl .== impl, :Latency],
        lab=cliq_names[impl])
end
savefig("graphics/cliq_normalization.pdf")

plot(xlabel="Cliquiness Theshold", ylabel="Latency (s)", title="Latency of Max-Clique Search")
for impl=levels(max_cliq_4.Impl)
    plot!(
        max_cliq_4[max_cliq_4.Impl .== impl, :Theshold],
        max_cliq_4[max_cliq_4.Impl .== impl, :Latency],
        lab=cliq_names[impl])
end
savefig("graphics/cliq_threshold.pdf")

plot(xlabel="Sample Percentage", ylabel="Latency (s)", title="Latency of Max-Clique Search")
for impl=levels(max_cliq_5.Impl)
    plot!(
        max_cliq_5[max_cliq_5.Impl .== impl, :Samples],
        max_cliq_5[max_cliq_5.Impl .== impl, :Latency],
        lab=cliq_names[impl])
end
savefig("graphics/cliq_samples.pdf")
