#![feature(slice_patterns)]

pub mod data;
use crate::data::*;

use failure::{format_err, Error};
use lazy_static::lazy_static;
use std::{
    cmp,
    collections::{BTreeMap, BTreeSet, HashMap},
    env,
};
use crossbeam::crossbeam_channel::{unbounded, Sender, Receiver};
use std::thread::spawn;
use std::sync::Arc;

struct Config {
    input_path: String,
    sample_percent: f64,
    cliquiness_normalization: f64,
    cliquiness_threshold: f64,
    max_jettison: usize,
}

impl Config {
    fn parse() -> Result<Config, Error> {
        let mut args = env::args().skip(1);
        let input_path = args.next().ok_or(format_err!("No input file given"))?;
        let sample_percent = args
            .next()
            .ok_or(format_err!("No sample percentage given"))?
            .parse::<f64>()
            .map_err(|_| format_err!("Sample percentage must be a float."))?;
        let cliquiness_normalization = args
            .next()
            .ok_or(format_err!("No cliquiness normalization given"))?
            .parse::<f64>()
            .map_err(|_| format_err!("Cliquiness normalization must be a float."))?;
        let cliquiness_threshold = args
            .next()
            .ok_or(format_err!("No cliquiness threshold given"))?
            .parse::<f64>()
            .map_err(|_| format_err!("Cliquiness threshold must be a float."))?;
        let max_jettison = args
            .next()
            .ok_or(format_err!("No max jettison given"))?
            .parse::<usize>()
            .map_err(|_| format_err!("Max jettison must be an integer."))?;
        Ok(Config {
            input_path,
            sample_percent,
            cliquiness_normalization,
            cliquiness_threshold,
            max_jettison,
        })
    }
}

lazy_static! {
    static ref CONFIG: Config = Config::parse().unwrap();
}

#[derive(Default)]
struct Builder {
    nodes: Vec<Node>,
    lookup: HashMap<String, usize>,
}

impl Builder {
    fn node(&mut self, label: &str) -> usize {
        match self.lookup.get(label) {
            Some(&ind) => ind,
            None => {
                self.nodes.push(Node { label: label.to_owned(), edges: BTreeSet::new() });

                let end = self.nodes.len() - 1;
                self.lookup.insert(label.to_owned(), end);
                end
            },
        }
    }
}

#[derive(Clone, Debug)]
pub struct Working {
    pub nodes: BTreeSet<usize>,
    pub frontier: BTreeMap<usize, usize>,
    pub edge_count: usize,
    pub connectivity: BTreeMap<usize, usize>,
    pub jettisoned: BTreeSet<usize>,
}

#[derive(Clone, Debug)]
pub struct PossibleClique {
    pub nodes: BTreeSet<usize>,
    pub cliquiness: f64,
}

impl cmp::Ord for PossibleClique {
    fn cmp(&self, other: &PossibleClique) -> cmp::Ordering {
        self.partial_cmp(other).unwrap()
    }
}

impl cmp::PartialOrd for PossibleClique {
    fn partial_cmp(&self, other: &PossibleClique) -> Option<cmp::Ordering> {
        Some(
            self.cliquiness
                .partial_cmp(&other.cliquiness)?
                .then_with(|| self.nodes.cmp(&other.nodes))
                .reverse(),
        )
    }
}

impl cmp::PartialEq for PossibleClique {
    fn eq(&self, other: &PossibleClique) -> bool {
        self.cmp(other) == cmp::Ordering::Equal
    }
}

impl cmp::Eq for PossibleClique {}

impl PossibleClique {
    fn from_working(working: &Working) -> PossibleClique {
        PossibleClique {
            nodes: working.nodes.clone(),
            cliquiness: working.cliquiness(CONFIG.cliquiness_normalization),
        }
    }
}

#[derive(Clone, Debug)]
pub struct MaximizeCliquiness(Working);

impl cmp::Ord for MaximizeCliquiness {
    fn cmp(&self, other: &MaximizeCliquiness) -> cmp::Ordering {
        self.partial_cmp(other).unwrap()
    }
}

impl cmp::PartialOrd for MaximizeCliquiness {
    fn partial_cmp(&self, other: &MaximizeCliquiness) -> Option<cmp::Ordering> {
        self.0
            .cliquiness(CONFIG.cliquiness_normalization)
            .partial_cmp(&other.0.cliquiness(CONFIG.cliquiness_normalization))
    }
}

impl cmp::PartialEq for MaximizeCliquiness {
    fn eq(&self, other: &MaximizeCliquiness) -> bool {
        self.cmp(other) == cmp::Ordering::Equal
    }
}

impl cmp::Eq for MaximizeCliquiness {}

impl Working {
    pub fn clone_and_append(&self, new_node: usize, graph: &Graph) -> Working {
        let mut new_working = self.clone();
        new_working.nodes.insert(new_node);

        // Remove this node from the frontier.
        let new_node_connectivity = new_working.frontier.remove(&new_node).unwrap();

        for (&k, v) in new_working.frontier.iter_mut() {
            if graph.nodes[k].edges.contains(&new_node) {
                *v += 1;
            }
        }

        // Calculate the edge_count, and insert nodes as necessary into the frontier.
        // Also determine the connectivity of each node.
        new_working.connectivity.insert(new_node, new_node_connectivity);

        for &node in graph.nodes[new_node].edges.iter() {
            if self.nodes.contains(&node) {
                new_working.edge_count += 1;
                *new_working.connectivity.entry(node).or_default() += 1;
            } else {
                new_working.frontier.insert(node, graph.nodes[node].edges.intersection(&new_working.nodes).count());
            }
        }

        new_working
    }

    pub fn remove_node(&self, remove: usize, graph: &Graph) -> Working {
        let mut new_working = self.clone();
        // Jetisoned are all of the things that we've removed from this set.
        new_working.jettisoned.insert(remove);

        // Remove the node from the node list.
        new_working.nodes.remove(&remove);

        // When we remove one node, we loose its connectivity from the edge count.
        new_working.edge_count = self.edge_count - self.connectivity[&remove];

        // Remove the node from the connectivity map.
        new_working.connectivity.remove(&remove);

        // Remove elements in the frontier which only neighbor the removed node.
        for &e in graph.nodes[remove].edges.iter() {
            // If the node has a node to the subgraph, decrement the subgraph element's
            // connectivity.
            if let Some(c) = new_working.connectivity.get_mut(&e) {
                *c -= 1
            }

            // If the node has a node to the frontier, decrement the frontier element's
            // connectivity.
            if let Some(f) = new_working.frontier.get_mut(&e) {
                *f -= 1
            }
        }

        // Go through to determine which things to remove from the frontier
        new_working.frontier = new_working.frontier.iter().map(|(&k, &v)| (k, v)).filter(|(_, v)| *v > 0).collect();

        //Working { nodes, frontier, edge_count, connectivity, jettisoned }
        new_working
    }

    pub fn new(a: usize, b: usize, graph: &Graph) -> Working {
        let mut nodes = BTreeSet::new();
        nodes.insert(a);
        nodes.insert(b);

        let mut frontier: BTreeMap<_, _> = graph.nodes[a]
            .edges
            .iter()
            .chain(graph.nodes[b].edges.iter())
            .map(|&n| (n, 0))
            .collect();

        frontier.remove(&a);
        frontier.remove(&b);

        for (k, v) in frontier.iter_mut() {
            if graph.nodes[a].edges.contains(k) {
                *v += 1;
            }

            if graph.nodes[b].edges.contains(k) {
                *v += 1;
            }
        }

        let mut connectivity = BTreeMap::new();
        connectivity.insert(a, 1);
        connectivity.insert(b, 1);

        Working { nodes, frontier, edge_count: 1, connectivity, jettisoned: BTreeSet::new() }
    }

    /// Calculates the cliquiness of a graph.
    fn cliquiness(&self, power: f64) -> f64 {
        // e = edge-count, n = node-count
        let e = self.edge_count as f64;
        let n = self.nodes.len() as f64;
        (2. * e) / ((n - 1.) * n.powf(power))
    }

    pub fn extensions<'a>(&'a self, graph: &'a Graph) -> impl Iterator<Item = Working> + 'a {
        // Consider both an extension of the subgraph and and (potentially) a removal of
        // a poorly connected node from the extensions.

        // Keep track of possible Working graphs with elements jettisoned.
        let possible_removals = self
            .connectivity
            .iter()
            .min_by_key(|(_, &v)| v)
            .map(|(&n, _)| self.remove_node(n, graph))
            // If we've jettisoned too many elements, ignore.
            .filter(|w| w.jettisoned.len() <= CONFIG.max_jettison)
            .into_iter();

        // Add the most connected node in the frontier.
        self.frontier
            .iter()
            .max_by_key(|(_, &v)| v)
            .map(|(&n, _)| self.clone_and_append(n, graph))
            .into_iter()
            .chain(possible_removals)
    }
}

fn line_to_edge(line: String, builder: &mut Builder) -> Option<(usize, usize)> {
    let mut words = line.split_whitespace();

    let a = builder.node(words.next()?);
    let b = builder.node(words.last()?);

    Some((a, b))
}

fn process_subgraphs(s_queue: Sender<Working>, r_queue: Receiver<Working>, graph: &Graph) -> Result<BTreeSet<PossibleClique>, Error> {
    let mut found = BTreeSet::new();
    while let Ok(work) = r_queue.try_recv() {
        for item in work.extensions(&graph)
                        .filter(|w| w.cliquiness(1.) > CONFIG.cliquiness_threshold)
                        .filter(|w| found.insert(PossibleClique::from_working(w)))
        {
            s_queue.send(item)?;
        }
    }

    Ok(found)
}

fn main() -> Result<(), Error> {
    use rand::{seq::IteratorRandom, thread_rng};
    use std::{
        fs::File,
        io::{prelude::*, BufReader},
    };

    let input_file = File::open(&CONFIG.input_path)?;
    let reader = BufReader::new(input_file);

    let mut builder = Builder::default();
    for line in reader.lines() {
        let (a, b) = line_to_edge(line?, &mut builder)
            .ok_or(format_err!("Line does not contain 2 entries!"))?;
        builder.nodes[a].edges.insert(b);
        builder.nodes[b].edges.insert(a);
    }

    // Remove self edges.
    for (i, node) in builder.nodes.iter_mut().enumerate() {
        node.edges.remove(&i);
    }

    let graph = Arc::new(Graph { nodes: builder.nodes });

    // Create an evaluation queue
    let (s_queue, r_queue) = unbounded();

    // Create a sample of the nodes in the graph.
    let mut rng = &mut thread_rng();
    let sample_size = (graph.nodes.len() as f64 * (CONFIG.sample_percent as f64 / 100.)) as usize;
    let sample: Vec<_> = (0..graph.nodes.len()).choose_multiple(&mut rng, sample_size);
    // eprintln!("{:?}", sample.iter().map(|i|
    // &graph.nodes[*i].label).collect::<Vec<_>>());

    // Put in the initial set of nodes into the set.
    for &a_id in sample.iter() {
        for b_id in graph.nodes[a_id].edges.iter() {
            s_queue.send(Working::new(a_id, *b_id, &graph))?;
        }
    }

    let threads: Vec<_> = (0..num_cpus::get()).map(|_| {
        let s_queue = s_queue.clone();
        let r_queue = r_queue.clone();
        let graph = graph.clone();
        spawn(move || {
            process_subgraphs(s_queue, r_queue, &*graph).unwrap()
        })
    }).collect();

    let found = threads.into_iter()
        .map(|t| t.join().unwrap())
        .fold(BTreeSet::new(), |mut a, mut b| {
            a.append(&mut b);
            a
        });

    eprintln!("========== Output ==========");
    /*
    for possible_clique in found.iter() {
        eprint!("{:.5} ", possible_clique.cliquiness);
        let possible_clique: Vec<_> =
            possible_clique.nodes.iter().map(|&i| &graph.nodes[i].label).collect();
        eprintln!("{:?}", possible_clique);
    }

    // Print out the best
    // if let Some(best) = found.iter().filter(|a| a.nodes.len() == 10).next() {
    if let Some(best) = found.iter().next() {
        for &a in best.nodes.iter() {
            for &b in graph.nodes[a].edges.intersection(&best.nodes) {
                println!("{} {}", graph.nodes[a].label, graph.nodes[b].label);
            }
        }
    }
    */

    Ok(())
}
