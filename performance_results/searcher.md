# Round 1

Sam's Oryx Pro
4 Search Threads
Diameter Limit 200
colorado-latest.osm.pbf
no simplification

## Branching Search
### No Dedup
Input Complete | in 5.207s
Found Starting Points | in 0.038s
Dispatch and Search | 150566.057 per s
Edges: 10671240
Searched: 380500
In Roundabout: 177934

Input Complete | in 4.828s
Found Starting Points | in 0.038s
Dispatch and Search | 162222.495 per s
Edges: 10671240
Searched: 380500
In Roundabout: 177934

Input Complete | in 4.881s
Found Starting Points | in 0.037s
Dispatch and Search | 160484.262 per s
Edges: 10671240
Searched: 380500
In Roundabout: 177934

### Dedup
Input Complete | in 5.086s
Found Starting Points | in 0.038s
Dispatch and Search | 56221.388 per s
Edges: 10671240
Searched: 380500
In Roundabout: 35020

Input Complete | in 4.966s
Found Starting Points | in 0.037s
Dispatch and Search | 56007.516 per s
Edges: 10671240
Searched: 380500
In Roundabout: 35020

Input Complete | in 5.121s
Found Starting Points | in 0.037s
Dispatch and Search | 56502.477 per s
Edges: 10671240
Searched: 380500
In Roundabout: 35020

### Hashbrown Dedup
Input Complete | in 5.182s
Found Starting Points | in 0.041s
Dispatch and Search | 47327.666 per s
Edges: 10671240
Searched: 380500
In Roundabout: 35020

Input Complete | in 5.247s
Found Starting Points | in 0.040s
Dispatch and Search | 50826.386 per s
Edges: 10671240
Searched: 380500
In Roundabout: 35020

Input Complete | in 4.841s
Found Starting Points | in 0.039s
Dispatch and Search | 51018.896 per s
Edges: 10671240
Searched: 380500
In Roundabout: 35020

### Random Samples
Input Complete | in 4.911s
Found Starting Points | in 0.039s
Dispatch and Search | 150795.787 per s
Edges: 10671240
Searched: 400000
In Roundabout: 186139

Input Complete | in 4.820s
Found Starting Points | in 0.040s
Dispatch and Search | 152869.553 per s
Edges: 10671240
Searched: 400000
In Roundabout: 184861

Input Complete | in 4.791s
Found Starting Points | in 0.041s
Dispatch and Search | 153876.574 per s
Edges: 10671240
Searched: 400000
In Roundabout: 186976

## Non-Branching Search
Input Complete | in 4.821s
Found Starting Points | in 0.039s
Dispatch and Search | 576718.730 per s (2.081s)
Edges: 10671240
Searched: 1200000
In Roundabout: 171402
Average Depth: 19.333
Average Branches: 21.195

Input Complete | in 4.869s
Found Starting Points | in 0.038s
Dispatch and Search | 570689.664 per s (2.103s)
Edges: 10671240
Searched: 1200000
In Roundabout: 171357
Average Depth: 19.355
Average Branches: 21.221

Input Complete | in 4.757s
Found Starting Points | in 0.038s
Dispatch and Search | 578309.909 per s (2.075s)
Edges: 10671240
Searched: 1200000
In Roundabout: 171316
Average Depth: 19.283
Average Branches: 21.146
