#!/usr/bin/python3

import sys
import numpy as np
import matplotlib.pyplot as plt
import ast

if len(sys.argv) < 2:
    print("Usage: {} <performance_file>...")
    sys.exit(-1)

inputs = []
speeds = []
for performance_file in sys.argv[1:]:
    print(performance_file)
    times = ast.literal_eval(open(performance_file, 'r').read())
    inputs = times.keys()
    speeds.append(list(times.values()))

ind = np.arange(len(inputs))
width = 0.2

for i, speed in enumerate(speeds):
    plt.bar(ind + (i*width), speed, width, align='center')
plt.title('Exhaustive subgraph discovery time for a 5-node world graph')
plt.xticks(ind, tuple(inputs))
plt.xlabel('Pattern graph')
plt.yticks(np.arange(0, 0.5, 0.05))
plt.ylabel('Time (s)')
plt.savefig('v1_graph.png', dpi=200)
plt.show()
