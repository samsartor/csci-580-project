use std::path::{Path, PathBuf};
use structopt::StructOpt;
use failure::{bail, Error};
use std::str::FromStr;
use std::fmt;
use std::io::prelude::*;
use std::collections::HashMap;

pub mod roads;
pub mod edges;
#[macro_use]
pub mod progress;

macro_rules! def_formats {
    {$name:ident, $set:ident, $($form:ident: [$short:expr $(, $other:expr)*],)*} => {
        #[derive(Copy, Clone, Debug, PartialEq, Eq)]
        enum $name {
            $($form,)*
        }

        impl FromStr for $name {
            type Err = Error;

            fn from_str(s: &str) -> Result<$name, Error> {
                Ok(match s.to_lowercase().as_str() {
                    $($short $(| $other)* => $name::$form,)*
                    _ => bail!("unknown format {}", s),
                })
            }
        }

        impl fmt::Display for $name {
            fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
                match *self {
                    $($name::$form => write!(f, $short),)*
                }
            }
        }

        static $set: &[&str] = &[$($short $(, $other)*,)*];
    }
}

def_formats! {
    OutFormat,
    OUT_FORMATS,
    RoadsJson: ["roads_json"],
    EdgesJson: ["edges_json"],
    RoadsBin: ["roads_bin"],
    EdgesBin: ["edges_bin"],
    RoadsSvg: ["roads_svg"],
    EdgesSvg: ["edges_svg"],
    EdgesTxt: ["edges_txt"],
    FlatEdgesText: ["flat_edges_txt"],
}

def_formats! {
    InFormat,
    IN_FORMATS,
    Json: ["json"],
    GzJson: ["json_gz"],
    OsmPbf: ["osm_pbf"],
}

impl InFormat {
    fn from_path(path: &Path) -> Option<InFormat> {
        let fname = path.file_name()?.to_str()?;
        Some(match () {
            () if fname.ends_with(".osm.pbf") => InFormat::OsmPbf,
            () if fname.ends_with(".json.gz") => InFormat::GzJson,
            () if fname.ends_with(".json") => InFormat::Json,
            _ => return None,
        })
    }

    fn input(self, reader: impl Read + Seek + Send) -> Result<roads::Roads, Error> {
        use self::InFormat::*;
        use serde_json::from_reader;
        use flate2::read::GzDecoder;
        use osmpbfreader::OsmPbfReader;

        match self {
            Json => from_reader(reader).map_err(Error::from),
            GzJson => from_reader(GzDecoder::new(reader)).map_err(Error::from),
            OsmPbf => {
                // load
                let mut roads = roads::Roads::new();
                roads.load(OsmPbfReader::new(reader))?;
                Ok(roads)
            }
        }
    }
}

impl OutFormat {
    fn output(self, mut roads: roads::Roads, simplify: bool, mut writer: impl Write) -> Result<(), Error> {
        use self::OutFormat::*;
        use serde_json::to_writer as write_json;
        use bincode::serialize_into as write_bin;

        if simplify { roads.reduce(); }

        match self {
            RoadsJson => {
                stage!("Writing Roads JSON Output");
                return write_json(writer, &roads).map_err(Error::from);
            },
            RoadsBin => {
                stage!("Writing Roads Bincode Output");
                return write_bin(writer, &roads).map_err(Error::from);
            },
            RoadsSvg => {
                stage!("Writing Roads SVG Output");
                return svg::write(&mut writer, &roads.to_svg()?).map_err(Error::from);
            },
            _ => (),
        }

        let mut edges = edges::Edges::new();
        match simplify {
            true => edges.append_roads_reduced(&roads)?,
            false => edges.append_roads(&roads)?,
        }

        stage!("Writing Edges Output");

        match self {
            RoadsJson | RoadsBin | RoadsSvg => unreachable!(),
            EdgesJson => write_json(writer, &edges).map_err(Error::from),
            EdgesBin => write_bin(writer, &edges).map_err(Error::from),
            EdgesTxt => {
                for (index, node) in edges.nodes.into_iter().enumerate() {
                    write!(writer, "{}", index)?;
                    for adj in node.edges {
                        write!(writer, " {:0.2} {}", adj.dist, adj.to)?;
                    }
                    writeln!(writer)?;
                    prog!(delta: 1);
                }
                Ok(())
            },
            FlatEdgesText => {
                for node in &edges.nodes {
                    for adj in &node.edges {
                        writeln!(writer, "{} {:0.2} {}", node.osmid, adj.dist, edges.nodes[adj.to].osmid)?;
                    }
                    prog!(delta: 1);
                }
                Ok(())
            },
            EdgesSvg => svg::write(&mut writer, &edges.to_svg()?).map_err(Error::from),
        }
    }
}

#[derive(Debug, StructOpt)]
#[structopt(name = "osm_street_graph", about = "Build a directed graph from OSM data.")]
struct Opt {
    #[structopt(parse(from_os_str))]
    input: PathBuf,
    #[structopt(parse(from_os_str))]
    output: Option<PathBuf>,
    #[structopt(short = "f", long = "out_format", default_value = "edges_json", help="output format")]
    #[structopt(raw(
        possible_values = "OUT_FORMATS",
        case_insensitive = "true",
    ))]
    out_format: OutFormat,
    #[structopt(short = "r", long = "in_format", help="override input format")]
    #[structopt(raw(
        possible_values = "IN_FORMATS",
        case_insensitive = "true",
    ))]
    in_format: Option<InFormat>,
    #[structopt(short = "S", long = "simplify", help="simplify road segments")]
    simplify: bool,
    #[structopt(short = "C", long = "compress", help="gzip compress output")]
    compress: bool,
    #[structopt(short = "t", long = "threads", help="override approx thread count")]
    threads: Option<usize>,
    #[structopt(short = "m", long = "drop_missing", help="drop missing node references")]
    drop_missing: bool,
}

fn main() -> Result<(), Error> {
    use std::fs::File;
    use std::io::stdout;
    use flate2::{write::GzEncoder, Compression};

    let opt = Opt::from_args();
    if let Some(threads) = opt.threads {
        rayon::ThreadPoolBuilder::new().num_threads(threads).build_global()?;
    }

    // input
    let form = match opt.in_format.or(InFormat::from_path(opt.input.as_ref())) {
        Some(form) => form,
        None => bail!("could not infer input format"),
    };
    let mut roads = form.input(File::open(opt.input)?)?;

    if opt.drop_missing {
        stage!("Filtering Node References");
        for road in &mut roads.roads {
            let nodes = &roads.nodes;
            road.nodes.retain(|&id| nodes[id as usize].pos.is_some());
        }

        let mut changes = HashMap::new();
        let mut i = 0;
        while i < roads.nodes.len() {
            if roads.nodes[i].pos.is_some() {
                i += 1;
            } else {
                roads.nodes.swap_remove(i);
                changes.insert(roads.nodes.len(), i);
            }
        }

        for r in &mut roads.roads {
            for n in &mut r.nodes {
                if let Some(&to) = changes.get(&(*n as usize)) {
                    *n = to as u64;
                }
            }
        }

        roads.roads.retain(|r| !r.nodes.is_empty());
    }

    // output
    let form = opt.out_format;
    match (opt.compress, opt.output) {
        (false, Some(path)) => form.output(roads, opt.simplify, File::create(path)?),
        (false, None) => form.output(roads, opt.simplify, stdout().lock()),
        (true, Some(path)) => form.output(roads, opt.simplify, GzEncoder::new(File::create(path)?, Compression::default())),
        (true, None) => form.output(roads, opt.simplify, GzEncoder::new(stdout().lock(), Compression::default())),
    }
}
